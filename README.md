# 1611 King James Version of the Holy Bible

Python tools for working with KJV text. Currently, it's only an API
for working with the text itself.

## Installation

- Check out the repo

        git clone https://dogwynn@bitbucket.org/dogwynn/kjv.git

- Use pip to install the `kjv` Python module

        cd kjv
        pip install .


## Example Usage

The current design provides a `Bible` object that is basically a
pointer to a `Verses` object for all the verses in the Bible. The
`Verses` object is the core data structure for the library.

```python
>>> import kjv
>>> bible = kjv.Bible()
>>> judges = bible.book('judg')
>>> judges.chapter(3)[10:12]
('Now Heber the Kenite, [which was] of the children of Hobab the \
father in law of Moses, had severed himself from the Kenites, and \
pitched his tent unto the plain of Zaanaim, which [is] by Kedesh.', \
'And they shewed Sisera that Barak the son of Abinoam was gone up to \
mount Tabor.')
>>> judges.chapter(3)[10]
'Now Heber the Kenite, [which was] of the children of Hobab the father \
in law of Moses, had severed himself from the Kenites, and pitched his \
tent unto the plain of Zaanaim, which [is] by Kedesh.'
```

- `Verses` objects:
    - Subclass of `tuple` containing `Verse` objects
    - Are returned from any slice of a `Verses` object
    - Have `book` and `chapter` methods to select 
- `Verse` objects are a subclass of `str`
- `Verse` objects have a few attributes and methods
  (e.g. `suggest_added`/`suggest_removed` that deal with the KJV
  "suggested" text).

```python
>>> v = judges.chapter(3)[10]
>>> type(v.book)
kjv.bible.Verses
>>> type(v.chapter)
kjv.bible.Verses
>>> v.b
'jg'
>>> v.c
3
>>> v.v
10
>>> v.suggest_removed()
'Now Heber the Kenite, of the children of Hobab the father in law of \
Moses, had severed himself from the Kenites, and pitched his tent unto \
the plain of Zaanaim, which by Kedesh.'
```

## TODO

- Integrate the Strong's concordance (Hebrew and Greek) data
- Provide a graph database for center column integration
- Better documentation
- Better unit tests




