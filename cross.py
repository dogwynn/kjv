from pathlib import Path

import networkx as nx

import kjv

book_map = {
    '1kgs': '1ki',
    '2kgs': '2ki',
    'phil': 'php',
    'phlm': 'phm',
}

def rows():
    lines = Path('cross_references.txt').read_text().splitlines()
    rows = [l.split() for l in lines[1:]]
    rows = [[r[0],r[1],int(r[2])] for r in rows]
    for i,row in enumerate(rows):
        for ri in [0,1]:
            row[ri] = row[ri].split('-')
            for j,v in enumerate(row[ri]):
                row[ri][j] = v.split('.')
                row[ri][j][0] = book_map.get(row[ri][j][0].lower(),
                                             row[ri][j][0])
                row[ri][j][0] = kjv.bible.book_name(row[ri][j][0])
                row[ri][j][1] = int(row[ri][j][1])
                row[ri][j][2] = int(row[ri][j][2])
    return rows
            
def graph(rows):
    G = nx.DiGraph()
    B = kjv.Bible()
    for i,row in enumerate(rows):
        source, dest, vote = row
        if len(dest)>1:
            (b0, c0, v0), (b1, c1, v1) = dest
            assert b0 == b1, '{}'.format(row)
            
