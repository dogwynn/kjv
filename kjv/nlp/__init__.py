from time import time
import bz2
import csv
import string
from functools import reduce, lru_cache
from pathlib import Path

from memoized_property import memoized_property
import pkg_resources
import nltk
from nltk.corpus import stopwords
from nltk.tokenize.moses import MosesDetokenizer

from .. import bible
from .. import data

memoize = lru_cache(1)

_nlp_data_paths = {
    'kjv_lemma': Path(data.paths.data,'kjv.lemma.tsv.bz2'),
    'bigrams': Path(data.paths.data,'bigrams.tsv.bz2'),
    'trigrams': Path(data.paths.data,'trigrams.tsv.bz2'),
    'quadgrams': Path(data.paths.data,'quadgrams.tsv.bz2'),
}
for key, path in _nlp_data_paths.items():
    if key not in data.paths:
        data.paths[key] = path

# Standard stop words
STOP = set(stopwords.words('english'))
# Stop words with no punctuation
STOP_NOPUNC = STOP | set(string.punctuation)

# Map of nltk.pos_tag tags to lemmatizer POS tags
POS2LEM = {
    'CC': 'n', 'CD': 'n', 'DT': 'n', 'EX': 'n', 'FW': 'n', 'IN': 'n',
    'JJ': 'a', 'JJR': 'a', 'JJS': 'a', 'LS': 'n', 'MD': 'n', 'NN': 'n',
    'NNP': 'n', 'NNPS': 'n', 'NNS': 'n', 'PDT': 'n', 'POS': 'n', 'PRP': 'n',
    'PRP$': 'n', 'RB': 'r', 'RBR': 'r', 'RBS': 'r', 'RP': 'n', 'SYM': 'n',
    'TO': 'n', 'UH': 'n', 'VB': 'v', 'VBD': 'v', 'VBG': 'v', 'VBN': 'v',
    'VBP': 'v', 'VBZ': 'v', 'WDT': 'n', 'WP': 'n', 'WP$': 'n', 'WRB': 'r',
}

def tokenized(verses):
    '''Given tuple of Verse objects, return generator of token lists

    Args:
      verses (Verses): Tuple of Verse objects to be tokenized

    Returns:
      generator (list[str]): Generator of lists of tokens for each
         verse

    '''
    for v in verses:
        yield nltk.word_tokenize(v.suggest_added())

def token_set(verses):
    '''Given a tuple of Verse objects, return a set of unique tokens

    Args:
      verses (Verses): Tuple of Verse objects to be tokenized

    Returns:
      set[str]: Set of unique tokens across all verses

    '''
    return reduce(lambda a,b:set(a)|set(b), tokenized(verses), set())    

def pos_tagged(verses):
    '''Given a tuple of Verse objects, return a generator of POS-tagged
    token lists

    Args:
      verses (Verses): Tuple of Verse objects to be tokenized

    Returns:
      generator (list[tuple[str]]): Generator of lists of POS-tagged
         tokens for each verse

    '''
    for tokens in tokenized(verses):
        yield nltk.pos_tag(tokens)

def lem_pos_tagged(verses):
    '''Given a tuple of Verse objects, return a generator of POS-tagged
    token lists (using lemmatize-appropriate POS tags)

    Args:
      verses (Verses): Tuple of Verse objects to be tokenized

    Returns:
      generator (list[tuple[str]]): Generator of lists of POS-tagged
         tokens for each verse

    '''
    for pos_tokens in pos_tagged(verses):
        yield [(tok, POS2LEM.get(pos,'n')) for tok, pos in pos_tokens]

def lemmatized(verses, stopwords=STOP_NOPUNC):
    '''Given a tuple of Verse objects, return a tuple of Verse objects
    whose text has been lemmatized

    The lemmatized text has had all stop words removed and all the
    words reduced to their appropriate root word.

    Args:
      verses (Verses): Tuple of Verse objects to be tokenized
      stopwords (list/set[str]): Set of stop words 

    Returns:
      Verses: Tuple of verses whose text has been lemmatized

    '''
    lem = nltk.stem.WordNetLemmatizer()
    lem_verses = []
    detokenizer = MosesDetokenizer()
    for i,(v,pos_tokens) in enumerate(zip(verses,lem_pos_tagged(verses))):
        lem_tokens = filter(
            lambda w: w.lower() not in stopwords,
            [lem.lemmatize(w, pos=pos) for w,pos in pos_tokens]
        )
        lem_tokens = detokenizer.detokenize(lem_tokens)
        v = Verse.from_data(v.bible, (v.b, v.c, v.v, ' '.join(lem_tokens)))
        lem_verses.append(v)
    return Verses(lem_verses)

def ngrams(verses, n):
    '''Given a tuple of Verse objects, return a generator of ngrams based
    on the verse text tokens

    Args:
      verses (Verses): Tuple of Verse objects to be ngrammed
      n (int): ngram dimension

    Returns:
      generator (tuple[str]): Generator of ngram tuples

    '''
    detokenizer = MosesDetokenizer()
    for tokens in tokenized(verses):
        # tokens = filter(lambda w: w.lower() not in stopwords, tokens)
        tokens = detokenizer.detokenize([w.replace('.','') for w in tokens])
        for ng in nltk.ngrams(tokens, n):
            yield ng

class Verses(bible.Verses):
    '''NLP oriented Verses object

    '''
    @memoized_property
    def tokens(self):
        '''Token set for this tuple of verses

        '''
        return token_set(self)

    @memoize
    def ngrams(self, n):
        '''Token set for this tuple of verses

        '''
        return ngrams(self, n)

class Verse(bible.Verse):
    '''NLP-oriented Verse object

    '''

def nlp_bible():
    return bible.Bible(verses_class=Verses, verse_class=Verse)
    

def lem_verse_generator():
    '''Returns an generator of verse tuples (book, chapter, verse, text)
    from the lemmatized version of the Bible

    '''
    with bz2.open(data.paths.kjv_lemma,'rt') as rfp:
        reader = csv.reader(rfp, delimiter='\t')
        for book,chapter,verse,text in reader:
            yield (book, int(chapter), int(verse), text)

def lemmatized_bible():
    return bible.Bible(lem_verse_generator,
                       verses_class=Verses, verse_class=Verse)

