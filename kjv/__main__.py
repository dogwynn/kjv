import argparse

import kjv

def get_args():
    parser = argparse.ArgumentParser(
        'Script environment for KJV module'
    )
    parser.add_argument('-v', '--verses', action='store_true')

    args = parser.parse_args()
    return args

def main():
    args = get_args()

    if args.verses:
        for verse in kjv.verse_iterator():
            print('\t'.join(map(str, verse)))
            
if __name__ == '__main__':
    main()
