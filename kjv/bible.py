import csv
import re
import bz2
from functools import lru_cache
from collections import OrderedDict

from memoized_property import memoized_property

from . import data

memoize = lru_cache(None)

BOOK_LUT = OrderedDict([
    # Old
    ('ge', 'Genesis'), ('ex', 'Exodus'), ('le', 'Leviticus'),
    ('nu', 'Numbers'), ('de', 'Deuteronomy'), ('jos', 'Joshua'),
    ('jg', 'Judges'), ('ru', 'Ruth'),
    ('1sa', '1 Samuel'), ('2sa', '2 Samuel'), ('1ki', '1 Kings'),
    ('2ki', '2 Kings'), ('1ch', '1 Chronicles'), ('2ch', '2 Chronicles'), 
    ('ezr', 'Ezra'), ('ne', 'Nehemiah'), ('es', 'Esther'), ('job', 'Job'),
    ('ps', 'Psalms'), ('pr', 'Proverbs'), ('ec', 'Ecclesiastes'),
    ('song', 'Song of Solomon'), ('isa', 'Isaiah'), ('jer', 'Jeremiah'),
    ('la', 'Lamentations'), ('eze', 'Ezekiel'), ('da', 'Daniel'),
    ('ho', 'Hosea'), ('joe', 'Joel'), ('am', 'Amos'), ('ob', 'Obadiah'),
    ('jon', 'Jonah'), ('mic', 'Micah'), ('na', 'Nahum'), ('hab', 'Habakkuk'),
    ('zep', 'Zephaniah'), ('hag', 'Haggai'), ('zec', 'Zechariah'),
    ('mal', 'Malachi'),
    # New
    ('mt', 'Matthew'), ('mr', 'Mark'), ('lu', 'Luke'), ('joh', 'John'), 
    ('ac', 'Acts'), ('ro', 'Romans'), ('1co', '1 Corinthians'),
    ('2co', '2 Corinthians'), ('ga', 'Galatians'), ('eph', 'Ephesians'),
    ('php', 'Philippians'), ('col', 'Colossians'),
    ('1th', '1 Thessalonians'), ('2th', '2 Thessalonians'),
    ('1ti', '1 Timothy'), ('2ti', '2 Timothy'), ('tit', 'Titus'),
    ('phm', 'Philemon'), ('heb', 'Hebrews'), ('jas', 'James'),
    ('1pe', '1 Peter'), ('2pe', '2 Peter'), ('1jo', '1 John'),
    ('2jo', '2 John'), ('3jo', '3 John'), ('jude', 'Jude'),
    ('re', 'Revelation'),
])
BOOK_ORDER = OrderedDict([ (k, i) for i,k in enumerate(BOOK_LUT) ])

BOOKS = OrderedDict([
    ('old', [
        'ge', 'ex', 'le', 'nu', 'de', 'jos', 'jg', 'ru', '1sa', '2sa',
        '1ki', '2ki', '1ch', '2ch', 'ezr', 'ne', 'es', 'job', 'ps',
        'pr', 'ec', 'song', 'isa', 'jer', 'la', 'eze', 'da', 'ho', 'joe',
        'am', 'ob', 'jon', 'mic', 'na', 'hab', 'zep', 'hag', 'zec', 'mal',
    ]),

    ('new', [
        'mt', 'mr', 'lu', 'joh', 'ac', 'ro', '1co', '2co', 'ga', 'eph',
        'php', 'col', '1th', '2th', '1ti', '2ti', 'tit', 'phm', 'heb',
        'jas', '1pe', '2pe', '1jo', '2jo', '3jo', 'jude', 're',
    ]),
])

def verse_generator():
    '''Returns a generator of verse tuples (book, chapter, verse, text)

    '''
    with bz2.open(data.paths.kjv,'rt') as rfp:
        reader = csv.reader(rfp, delimiter='\t')
        for book,chapter,verse,text in reader:
            yield (book, int(chapter), int(verse), text)

class KjvBookNameError(Exception):
    pass

@memoize
def book_name(frag):
    '''Covert fragment to correct book key

    - If fragment is a key, return it.
    - If fragment (minus whitespace) is a prefix for *only one* full
      book name (minus whitespace), return the key associated with
      that book
    - Otherwise, throw an error

    '''
    frag = frag.replace(' ','').lower()
    if frag in BOOK_LUT:
        return frag
    matched = None
    for key, full_name in BOOK_LUT.items():
        search_name = full_name.replace(' ','').lower()
        if search_name.startswith(frag):
            if matched is not None:
                raise KjvBookNameError(
                    'More than one name matched for {}: ({}, {})'.format(
                        frag, BOOK_LUT[matched], BOOK_LUT[key],
                    )
                )
            matched = key
    if matched is None:
        raise KjvBookNameError(
            'No books were matched for {}'.format(frag)
        )
    return matched

_suggest_re = re.compile(r'\s*\[(.*?)\]')
_suggest_chars_re = re.compile(r'[\[\]]')
_comment_re = re.compile(r'<<(.*?)>>')

class Verse(str):
    '''Verse string with metadata and references to containing Book and
    Chapter objects

    Attributes:

    - bible (Bible): reference to Bible object
    - b (str): book key
    - c (int): chapter number
    - v (int): verse number

    '''
    @classmethod
    def from_data(cls, bible, data):
        '''Generate a verse object from the raw TSV row tuples 

        Args:

          bible (Bible): Bible object creating the verse
          data (tuple): (book key, chapter number, verse number, verse
             text) created from the raw TSV rows

        '''
        book, chapter, verse, text = data
        new = cls(text)
        new.bible = bible
        new.b = book
        new.c = chapter
        new.v = verse
        return new

    @property
    def book(self):
        '''Reference to containing Book

        Example:

        >>> v = bible.verses[0]
        >>> v
        'In the beginning God created the heaven and the earth.'
        >>> v.book.chapter(0)[1]
        'And the earth was without form, and void; and darkness [was]
        upon the face of the deep. And the Spirit of God moved upon
        the face of the waters.'

        '''
        return self.bible.book(self.b)

    @property
    def chapter(self):
        '''Reference to containing Chapter

        Example:

        >>> v = bible.verses[0]
        >>> v
        'In the beginning God created the heaven and the earth.'
        >>> v.chapter[1]
        'And the earth was without form, and void; and darkness [was]
        upon the face of the deep. And the Spirit of God moved upon
        the face of the waters.'

        '''
        return self.bible.book(self.b).chapter(self.c)

    
    def suggest_removed(self):
        '''Generated a Verse object with KJV suggestions removed

        Example:

        >>> v = bible.verses[1]
        >>> v
        'And the earth was without form, and void; and darkness [was]
        upon the face of the deep. And the Spirit of God moved upon
        the face of the waters.'
        >>> v.suggest_removed()
        'And the earth was without form, and void; and darkness upon
        the face of the deep. And the Spirit of God moved upon the
        face of the waters.'

        '''
        return self.__class__.from_data(
            self.bible, (self.b, self.c, self.v, _suggest_re.sub('', self))
        )

    def suggest_added(self):
        '''Generated a Verse object with KJV suggestions added as text

        Example:

        >>> v = bible.verses[1]
        >>> v
        'And the earth was without form, and void; and darkness [was]
        upon the face of the deep. And the Spirit of God moved upon
        the face of the waters.'
        >>> v.suggest_removed()
        'And the earth was without form, and void; and darkness was
        upon the face of the deep. And the Spirit of God moved upon
        the face of the waters.'
        '''
        return self.__class__.from_data(
            self.bible,
            (self.b, self.c, self.v, _suggest_chars_re.sub('', self))
        )

class Verses(tuple):
    '''Collection (tuple) of Verse objects
    '''
    def __getitem__(self, key):
        if isinstance(key, slice):
            return self.__class__(super().__getitem__(key))
        else:
            return super().__getitem__(key)

    @memoize
    def suggest_removed(self):
        '''Tuple of Verse objects with KJV suggestions removed

        '''
        return self.__class__(v.suggest_removed() for v in self)

    @memoize
    def suggest_added(self):
        '''Tuple of Verse objects with KJV suggestions added as text

        '''
        return self.__class__(v.suggest_added() for v in self)
    

    @memoized_property
    def books_set(self):
        '''Set of all book keys (strings) referenced in this tuple of verses

        '''
        return sorted({v.b for v in self}, key=lambda b:BOOK_ORDER[b])

    @memoized_property
    def chapters_set(self):
        '''Set of all chapters referenced in this tuple of verses

        Chapters in this context are tuples of (book key, chapter
        number) pairs.

        '''
        return sorted({(v.b, v.c) for v in self},
                      key=lambda t: (BOOK_ORDER[t[0]],t[1]))

    @memoized_property
    def verses_set(self):
        '''Set of all verses referenced in this tuple of verses

        Verses in this context are (book key, chapter number, verse
        number) triples.

        '''
        return sorted({(v.b, v.c, v.v) for v in self},
                      key=lambda t: (BOOK_ORDER[t[0]],t[1],t[2]))

    @property
    def books(self):
        for book in self.books_set:
            yield self.book(book)

    @property
    def chapters(self):
        for (book, chapter) in self.chapters_set:
            yield self.book(book).chapter(chapter)

    @memoize
    def book(self, *names):
        '''Tuple of Verse objects of given book name

        If the name matches a book key, that is returned. Otherwise,
        names can be given in a pseudo-fuzzy manner. E.g. '1chron' or
        '1chr' matches '1 Chronicles'. If the name matches more than
        one book, then an error is raised.

        Args:
          name (str): name of book

        Returns:
          Verses: Tuple of Verse objects for the given book

        '''
        books = {book_name(name) for name in names}
        books_set = set(self.books_set)
        if not books.issubset(self.books_set):
            missing = books - books_set
            raise KeyError(
                'The book{} ({}) {} not contained in these verses'.format(
                    's' if len(missing)>1 else '',
                    ', '.join(sorted(missing)),
                    'is' if len(missing)==1 else 'are'
                )
            )
        return self.__class__(v for v in self if v.b in books)

    @memoize
    def chapter(self, *chapters):
        '''Tuple of Verse objects of given chapter number

        Args:
          *chapters (int): chapter numbers. Can be given as negative
            index

        Returns: Verses tuple of Verse objects

        '''
        verses = []
        for book in self.books_set:
            book_verses = self.book(book)
            for i,c in enumerate(chapters):
                if c < 0:
                    chapters[i] = len(book_verses) + c
            chapters = set(chapters)
            all_chapters = {c for b,c in self.chapters_set if b==book}
            if not chapters.issubset(all_chapters):
                missing = chapters - all_chapters
                raise KeyError(
                    'There {} no chapter{} ({}) in these verses'.format(
                        'is' if len(missing)==1 else 'are',
                        's' if len(missing)>1 else '',
                        ', '.join(sorted(missing))
                    )
                )
            verses.extend(v for v in book_verses if v.c in chapters)
        return self.__class__(verses)

    @memoize
    def verse(self, *verses):
        '''Tuple of Verse objects corresponding to given verses

        Args:
          *verses (int): Verse numbers.

        Returns: Verses tuple of Verse objects

        '''
        all_verses = []
        for (book,chapter) in self.chapters_set:
            chapter_verses = self.book(book).chapter(chapter)
            all_verses.extend(v for v in chapter_verses if v.v in verses)
        return self.__class__(all_verses)

class Bible:
    '''KJV Bible reference object

    Provides a reference point for all verses in the Bible.

    Attributes:
      verses (Verses): Tuple of all verses as Verse objects

    '''

    def __init__(self, generator=None, *,
                 verses_class=Verses, verse_class=Verse):
        generator = generator or verse_generator

        self.verses = verses_class(
            verse_class.from_data(self, v) for v in generator()
        )

    @property
    def books(self):
        for book in BOOK_LUT:
            yield self.book(book)

    def book(self, *names):
        '''Tuple of Verse objects of given book name

        If the name matches a book key, that is returned. Otherwise,
        names can be given in a pseudo-fuzzy manner. E.g. '1chron' or
        '1chr' matches '1 Chronicles'. If the name matches more than
        one book, then an error is raised.

        Args:
          name (str): name of book

        Returns:
          Verses: Tuple of Verse objects for the given book

        '''
        return self.verses.book(*names)

    def chapter(self, book, *chapters):
        '''Tuple of Verse objects of given book name and chapter number

        Args:
          book (str): name of book
          chapter (int): chapter number

        Returns:
          Verses: Tuple of Verse objects for the given chapter

        '''
        return self.verses.book(book).chapter(*chapters)

@memoize
def get_bible():
    return Bible()
