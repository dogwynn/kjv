import pkg_resources

from . import data
from . import bible
from . import nlp

from .bible import (
    get_bible, Bible, Verse, Verses,
)

