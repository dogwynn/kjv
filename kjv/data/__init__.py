from pathlib import Path

import pkg_resources
from gems import composite

paths = composite({
    'data': Path(pkg_resources.resource_filename('kjv', 'data/')),
    'kjv': Path(pkg_resources.resource_filename('kjv', 'data/kjv.tsv.bz2')),
})
