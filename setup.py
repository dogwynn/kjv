from pathlib import Path
from distutils.core import setup

HERE = Path(__file__).resolve().parent

long_description = Path(HERE, 'README.md').resolve().read_text()

setup(
    name='kjv',
    packages=['kjv'],

    package_data={
        'kjv': ['data/*.bz2'],
    },

    install_requires=Path(
        HERE, 'requirements.txt'
    ).read_text().strip().splitlines(),

    version='0.0.1',

    description='Library for reading/analysis of 1611 KJV Bible',
    long_description=long_description,

    author="Gabe Monroe, David O'Gwynn",
    author_email="jgmonroe.phd@gmail.com, dogywnn@acm.org",

    classifiers=[
        'Programming Language :: Python',
    ],
)
